import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TrainerInfoComponent } from './trainer-info/trainer-info.component';
import { ListTrainerComponent } from './list-trainer/list-trainer.component';
import { EquiposComponent } from './equipos/equipos.component';

@NgModule({
  declarations: [
    AppComponent,
    TrainerInfoComponent,
    ListTrainerComponent,
    EquiposComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
