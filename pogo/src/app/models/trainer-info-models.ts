export class TrainerInfo {
    avatar: string;
    email: string;
    team: string

    constructor(a: string, e: string, t: string) {
        this.avatar = a;
        this.email = e;
        this.team = t;
    }
}