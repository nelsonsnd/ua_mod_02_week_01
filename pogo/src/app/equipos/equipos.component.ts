import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-equipos',
  templateUrl: './equipos.component.html',
  styleUrls: ['./equipos.component.css']
})
export class EquiposComponent implements OnInit {
  equipos: string[];
  constructor() { 
    this.equipos=['Team Valor','Team Mystic', 'Team Instinct'];
  }

  ngOnInit() {
  }

}
