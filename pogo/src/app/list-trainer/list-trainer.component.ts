import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-trainer',
  templateUrl: './list-trainer.component.html',
  styleUrls: ['./list-trainer.component.css']
})
export class ListTrainerComponent implements OnInit {
  equipos: string [];
  constructor() {
    this.equipos = ['Team Valor', 'Team Mystic', 'Team Instinct'];
   }

  ngOnInit() {
  }

}
